# Bash Exercises Readme
#### Notes on Bash and POSIX shells
### [Bash Manual](https://www.gnu.org/software/bash/manual/bashref.html)

## Shell Expansion Syntax

## Single [] and Double [[]] Brackets
Single brackets are a POSIX standard. This implies greater portability between different shells such as csh, ksh, zsh, etc.
Double brackets are a "bashism" that enables use of:
- Comparison operators: < > <= >= == && ||
- Regular expression comparator: =~

[ is the name of an actual binary executable on *NIX
    
    $ which [
    /usr/bin/[
    
likewise, true and false
    
    $ for binary in true false; do which $binary; done
    /usr/bin/true
    /usr/bin/false

## Arithmetic Functions - (()) and let
Double parenthesis are the bash way of expressing arithmetic operations. 
Variables are addressed without the use of $
'let' functions in a similar manner to (())

    let foo+=2
    (( foo + = 2 ))

### (()) Inherent T/F Evaluation

    $ if (( `echo 1` )); then echo "true"; else echo "false"; fi
    true
    $ if (( `echo 0` )); then echo "true"; else echo "false"; fi
    false
    
    $ if [[ `echo 1` ]]; then echo "true"; else echo "false"; fi
    true
    $ if [[ `echo 0` ]]; then echo "true"; else echo "false"; fi
    **true**
  
Square bracket evaluation requires a test
  
    $ if [[ `echo 0` == 1 ]]; then echo "true"; else echo "false"; fi
    false

### Floating Point Math
Bash isn't capable of doing floating point arithmetic.
It is possible to use basic calculator for that.
bc - an arbitrary precision calculator

## Summary: String Manipulation and Expanding Variables [nixCraft](https://www.cyberciti.biz/tips/bash-shell-parameter-substitution-2.html)
   
${parameter:-defaultValue}	Get default shell variables value

${parameter:=defaultValue}	Set default shell variables value

${parameter:?"Error Message"}	Display an error message if parameter is not set

${#var}	Find the length of the string

${var%pattern}	Remove from shortest rear (end) pattern

${var%%pattern}	Remove from longest rear (end) pattern

${var:num1:num2}	Substring

${var#pattern}	Remove from shortest front pattern

${var##pattern}	Remove from longest front pattern

${var/pattern/string}	Find and replace (only replace first occurrence)

${var//pattern/string}	Find and replace all occurrences

${!prefix*} Expands to the names of variables whose names begin with prefix

${var,}   Convert first character to lowercase

${var,,}  Convert all characters to lowercase

${var^}   Convert first character to uppercase

${var^^}  Convert all character to uppercase

${var~}   Convert case of first character

${var~~}  Convert case of all characters

## Special Parameters [Manual](https://www.gnu.org/software/bash/manual/html_node/Special-Parameters.html)
($*) Expands to the positional parameters, starting from one. When the expansion is not within double quotes, each positional parameter expands to a separate word. In contexts where it is performed, those words are subject to further word splitting and pathname expansion. When the expansion occurs within double quotes, it expands to a single word with the value of each parameter separated by the first character of the IFS special variable. That is, "$*" is equivalent to "$1c$2c…", where c is the first character of the value of the IFS variable. If IFS is unset, the parameters are separated by spaces. If IFS is null, the parameters are joined without intervening separators.

($@) Expands to the positional parameters, starting from one. When the expansion occurs within double quotes, each parameter expands to a separate word. That is, "$@" is equivalent to "$1" "$2" …. If the double-quoted expansion occurs within a word, the expansion of the first parameter is joined with the beginning part of the original word, and the expansion of the last parameter is joined with the last part of the original word. When there are no positional parameters, "$@" and $@ expand to nothing (i.e., they are removed).

($#) Expands to the number of positional parameters in decimal.

($?) Expands to the exit status of the most recently executed foreground pipeline. (Confusingly, false statements exit with code 1 and true statements exit code 0)

($-, a hyphen.) Expands to the current option flags as specified upon invocation, by the set builtin command, or those set by the shell itself (such as the -i option).

($$) Expands to the process ID of the shell. In a () subshell, it expands to the process ID of the invoking shell, not the subshell.

($!) Expands to the process ID of the job most recently placed into the background, whether executed as an asynchronous command or using the bg builtin (see Job Control Builtins).

($0) Expands to the name of the shell or shell script. This is set at shell initialization. If Bash is invoked with a file of commands (see Shell Scripts), $0 is set to the name of that file. If Bash is started with the -c option (see Invoking Bash), then $0 is set to the first argument after the string to be executed, if one is present. Otherwise, it is set to the filename used to invoke Bash, as given by argument zero.

($_, an underscore.) At shell startup, set to the absolute pathname used to invoke the shell or shell script being executed as passed in the environment or argument list. Subsequently, expands to the last argument to the previous command, after expansion. Also set to the full pathname used to invoke each command executed and placed in the environment exported to that command. When checking mail, this parameter holds the name of the mail file. 

## Index Arrays
    $ arr=(foo bar);
     
    $ echo ${arr[@]} ${arr[*]} "${arr[@]}" "${arr[*]}"
    foo bar
    foo bar
    foo bar
    foo bar
    
   note the white space is not recognized when using * and doulequotes
    
    $ for i in "${arr[*]}" ; do echo $i ; done; \
      for i in ${arr[*]}   ; do echo $i ; done;
    foo bar
    foo
    bar
    
    S for i in "${arr[@]}" ; do echo $i ; done; \
      for i in ${arr[@]}   ; do echo $i ; done;
    foo
    bar
    foo
    bar

## Associative Arrays
    declare -A associativeArray=( ["a"]=1 ["b"]=2 ["c"]=3 )
    associativeArray+=( ["d"]=4 ["e"]=5 ["f"]=6 )
    echo "the values are " ${associativeArray[@]}
    echo "the keys are   " ${!associativeArray[@]}

## Variable Indirection Expansion
Used to dynamically name variables.

    $ var1=dynamicName
    $ dynamicName=someValue
    $ echo ${var1}  # normal, var1 to dynamicName
    dynamicName
    $ echo ${!var1} # indirection, var1 to dynamicName to someValue
    someValue

## Redirect stderr to stdout
    $ command 2>&1
    
## Strange Arithmetic
### Watch out! - 10 is sometimes less than 2

    $ #!/usr/bin/env bash
    $ function print() { (( $1 )) && printf X || printf .; }
    $ for n in 1 2 9 10 20 'a'; do
    $ (( $n < 2 )); print $?
    $ [[ $n < 2 ]]; print $?
    $ (( $n > 2 )); print $?
    $ [[ $n > 2 ]]; print $?
    $ printf "\t$n\n"
    $ done
    ..XX    1
    XXXX    2
    XX..    9
    X..X    10
    XX..    20
    .XX.    a
    
<!--more-->

### Watch out - case matching with "case"

    $ #!/usr/bin/env bash
    $ echo "   cc ^^ ,,"
    $ 
    $ for c in 'a' 'A' 'z' 'Z'; do
    $ printf "$c  "
    $ 
    $ case $c in
    $ [A-Z]) printf X ;;
    $ [a-z]) printf . ;;
    $ esac
    $ case $c in
    $ [a-z]) printf . ;;
    $ [A-Z]) printf X ;;
    $ esac
    $ printf " "
    $ 
    $ case ${c^} in
    $ [A-Z]) printf X ;;
    $ [a-z]) printf . ;;
    $ esac
    $ case ${c^} in
    $ [a-z]) printf . ;;
    $ [A-Z]) printf X ;;
    $ esac
    $ printf " "
    $ 
    $ case ${c,} in
    $ [A-Z]) printf X ;;
    $ [a-z]) printf . ;;
    $ esac
    $ case ${c,} in
    $ [a-z]) printf . ;;
    $ [A-Z]) printf X ;;
    $ esac
    $ 
    $ printf "\n"
    $ done
       cc ^^ ,,
    a  .. X. ..
    A  X. X. ..
    z  X. XX X.
    Z  XX XX X.

## Setting Input Arguments
    $ set -- foo bar
    $ echo $1 $2
    foo bar
    $ $1
    bash: foobar: command not found
    $ !$
    $1
    bash: foobar: command not found
    $ echo
    
    $ $1
    bash: foobar: command not found
    $ echo
$ !$
echo

$ !*

$ 


$ set -- "   "
$ echo $1a
a
$ echo a$1a
a a
$ echo "a$1a"
a   a

# echo literal newline
echo $'\n'

$ set -- $'a   a \t a'
$ echo "$1"
a   a    a
$ echo $1
a a a
$ 

$ set -- $'a b\tc\rd' 'abc'
$ echo "$1"
d b     c

$ function print() { (( $1 )) && printf X || printf .; }
$ 
$ for n in 1 2 9 10 20 'a'; do
$ (( $n < 2 )); print $?
$ [[ $n < 2 ]]; print $?
$ (( $n > 2 )); print $?
$ [[ $n > 2 ]]; print $?
$ printf "\t$n\n"
$ done

..XX    1
XXXX    2
XX..    9
X..X    10
XX..    20
.XX.    a

## Case Collation
$ for c in 'a' 'A' 'z' 'Z'; do
$ case $c in
$ [a-z]) echo "lower" ;;
$ [A-Z]) echo "upper" ;;
$ esac
$ done
lower
lower
lower
upper

# What is this syntax: ```done < <(echo -n "$1")```
while read -n1 character; do
  if (( ${#character} == 0 )); then
    reverse="${character} ${reverse}"
  else
    reverse="${character}${reverse}"
  fi
done < <(echo -n "$1")
