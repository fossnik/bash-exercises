#!/usr/bin/env bash

# create acronym (NAA = Non-Alphabetical Antecedent)
for (( i = 0, NAA = 1; i < ${#1}; ++i )); do
  case "${1:$i:1}" in
    [a-Z])
      if [[ $NAA == 1 ]]; then
        acronym="$acronym"${1:$i:1}
        NAA=0
      fi
    ;;
    *) # non-alphabetical character detected
      NAA=1
    ;;
  esac
done

# output upper-case acronym
echo ${acronym^^}
