#!/usr/bin/env bash

# create associative array for the keyword
declare -A keywordLetters

# array to store verified pangrams
declare -a pangrams

for (( i = 0; i < ${#1}; i++ ))
do # tally instances of letters in keyword
  ((keywordLetters[`echo ${1:$i:1} | awk '{print tolower($0)}'`]++))
done

for testWord in $2
do # loop through each test word
  declare -A testwordLetters

  for (( i = 0; i < ${#testWord}; i++ ))
  do # tally instances of letters in test word
    ((testwordLetters[`echo ${testWord:$i:1} | awk '{print tolower($0)}'`]++))
  done

  for letter in ${!keywordLetters[@]}
  do # test each letter in keyword for equivalent quantity in testword
    if [[ ! ${!keywordLetters[$letter]} == ${!testwordLetters[$letter]} ]]
    then
      isPangram='F'
      break
    fi
  done

  if [[ $isPangram != 'F' && ${#testWord} == ${#1} && ${testWord^^} != ${1^^} ]]
    then pangrams+=($testWord)
  fi

  unset isPangram
  unset testwordLetters
done

echo ${pangrams[@]}
