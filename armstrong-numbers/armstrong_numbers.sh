#!/usr/bin/env bash
  
exp=${#1}
sum=0

# loop through each character
for (( i=0 ; i<${exp} ; i++ )); do
  ((sum+=${1:i:1}**${exp}))
done

if [[ $sum -eq $1 ]]; then
  echo "true"
else
  echo "false"
fi
