#!/usr/bin/env bash

transpose(){
  # declare lowercase string (converts to lowercase)
  declare -l local char=$1

  case $char in
    [a-z])
    # get ASCII value
    local ascii=$(printf "%d" "'$char")

    # get index of the letter substitute
    ((ascii=25+97-ascii+97))

    # convert number back to letter by for some random reason using octal
    printf \\$(printf "%o" $ascii)
    ;;

    # print non-letters as-is
    *) printf $char ;;
  esac
}

case $1 in
"encode")
 for c in $(fold -w1 <<< ${2//[^0-9a-zA-Z]/}); do
   # must use space after each 5th character
   (( fifths++ % 5 == 0 && fifths != 1 )) && printf " "
   transpose $c
 done
;;

"decode")
 for c in $(fold -w1 <<< ${2//[^0-9a-zA-Z]/})
 do transpose $c
 done
;;

"*") exit 1;;
esac
