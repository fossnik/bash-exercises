#!/usr/bin/env bash

function isAllCaps() {
  # false if contains no letters
  if [[ ! $1 == *[a-zA-Z]* ]]
  then return 0
  fi

  for (( i = 0; i < ${#1}; ++i )); do
    if [[ ${1:$i:1} =~ [a-z] ]]
    then return 0
    fi
  done

  return 1
}

function announce() {
  echo $1
  exit
}

isAllCaps ${1//[^a-zA-Z]}
allCaps=$?

noWhitespace=${1//[$' \t\n\r']/}
lastCharacter=${noWhitespace:(-1)}

# silence
if [[ $noWhitespace == "" ]]
then announce "Fine. Be that way!"

elif [[ $lastCharacter == "?" ]]
then
if (( $allCaps ))
then announce "Calm down, I know what I'm doing!"
else announce "Sure."
fi

elif (( $allCaps ))
then announce "Whoa, chill out!"
fi

announce "Whatever."
