#!/usr/bin/env bash

if (( $1 < 1 )); then
  echo "Error: Only positive numbers are allowed"
  exit 1
elif (( $1 == 1 )); then
  echo 0
fi

for (( n = $1; n != 1; ++steps )); do
  if (( $n % 2 == 0 ))
  then ((n = n / 2))
  else ((n = n * 3 + 1))
  fi
done

echo $steps
