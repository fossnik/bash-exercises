#!/usr/bin/env bash

# get ASCII values
start=$(printf "%d" "'A")
end=$(printf "%d" "'$1")

((range=$end-$start)) # size of alphabetical range
((width=$range*2+1))  # width of rows

function printRow() {
  local rowDepth=$1-$start
  for (( col = 0; col < $width; col++ )); do
    if (( col + rowDepth == range || col == rowDepth + range ))
    then printf \\$(printf "%o" $1)
    else printf " "
    fi
  done
  echo
}

# ascending
for (( i = $start; i < $end; i++ ))
do printRow $i
done

## descending
for (( i = $end; i >= $start; i-- ))
do printRow $i
done
