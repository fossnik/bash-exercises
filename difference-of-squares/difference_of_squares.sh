#!/usr/bin/env bash

for (( i=0 ; i<=$2 ; i++ )); do

 let sum+=i
 let sumSqrs+=$((i**2))

done

sumSquared=$((sum**2))

if [[ "$1" == "square_of_sum" ]]; then
  echo $sumSquared

elif [[ "$1" == "sum_of_squares" ]]; then
  echo $sumSqrs

elif [[ "$1" == "difference" ]]; then
  echo $((sumSquared-sumSqrs))

fi