#!/usr/bin/env bash

# tr remove double spaces (for some reason there are double spaces between Month and Day)
date -u --date="$1 + 1000000000 seconds" | tr -s ' '
