#!/usr/bin/env bash

grains=1

if [[ "$1" == "total" ]]; then
 printf 18446744073709551615
 exit 0
fi

if [[ "$1" -gt 64 ]] || [[ "$1" -lt 1 ]]; then
 printf "Error: invalid input"
 exit 1
fi

if [[ "$1" -eq 64 ]]; then
 printf 9223372036854775808
 exit 0
fi

for (( square=1; square<$1; square++)); do
 let "grains*=2"
done

printf $grains
