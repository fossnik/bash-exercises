#!/usr/bin/env bash

if [[ ${#1} -ne ${#2} ]]; then
  echo "left and right strands must be of equal length"
  exit 1
fi

if [[ ${#@} -ne 2 ]]; then
  echo "Usage: hamming.sh <strand1> <strand2>"
  exit 1
fi

# find mismatches
for (( i = 0, mismatches = 0; i < ${#1}; i++ )); do
  if [[ ${1:i:1} != ${2:i:1} ]]; then
    let "mismatches++"
  fi
done

echo $mismatches
