#!/usr/bin/env bash

# strip punctuation
input=${1//[^0-9X]/}

# must be length ten
(( ${#input} != 10 )) || echo false && exit

# add ten for check
[[ ${input:9:1} == "X" ]] && ((sum = 10))

for (( index = 10; $index < ${#input}; index-- ))
do (( sum += $c * $index ))
done

(( $sum % 11 == 0 )) && echo true || echo false
