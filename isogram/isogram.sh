#!/usr/bin/env bash

for c in $(fold -w1 <<< ${1//[^a-zA-Z]/})
do
if [[ ! $letters =~ ${c,} ]]
then letters="$letters${c,}"
else echo false; exit
fi
done

echo true
