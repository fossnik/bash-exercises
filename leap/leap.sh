#!/usr/bin/env bash

fail() {
  echo 'Usage: leap.sh <year>'
  exit 1
}

# test for proper number of input args
if [[ ${#@} -ne 1 ]]
  then fail
fi

# test that all input is digits
re='^[0-9]+$'
if ! [[ $1 =~ $re ]]
  then fail
fi

# determine whether leap or not
if (($1 % 400 == 0))
  then echo "true"

elif (($1 % 100 == 0))
  then echo "false"

elif (($1 % 4 == 0))
  then echo "true"

else
  echo "false"
fi
