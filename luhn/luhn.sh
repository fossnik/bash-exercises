#!/usr/bin/env bash

# remove whitespace
input=${1//[^0-9]/}

# single digits are invalid
(( ${#input} < 2 )) && echo false && exit

# punctuation is invalid
re='^[0-9 ]+$'
[[ ! $1 =~ $re ]] && echo false && exit

for c in $(fold -w1 <<< ${1//[^0-9]/}); do
  if (( $flip )); then
    flip=0
    if (( $c * 2 > 9 ))
    then ((sum += $c * 2 - 9))
    else ((sum += $c * 2))
    fi
  else
    flip=1
    ((sum += $c))
  fi
done

if (( $sum % 10 == 0 ))
then echo true
else echo false
fi
