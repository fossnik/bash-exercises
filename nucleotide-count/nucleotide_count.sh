#!/usr/bin/env bash

# use associative array
declare -A tally=(["G"]=0 ["C"]=0 ["T"]=0 ["A"]=0)

# tally up nucleotides
for c in $(fold -w1 <<< $1)
do [[ ! "GCTA" =~ "$c" ]] && echo "Invalid nucleotide in strand" && exit 1 \
    || ((tally[$c]++))
done

for i in "${!tally[@]}"
do echo $i: ${tally[$i]}
done
