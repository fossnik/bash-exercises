#!/usr/bin/env bash

# Check if the given string is a pangram

# zero-length input argument is false
if [[ ${#1} == 0 ]]; then
  echo false
  exit 0
fi

# regex match test if each letter of alphabet can be found in the (lowercase) input
for letter in {a..z}; do
  if [[ ! ${1,,} =~ .*${letter}.* ]]; then
    echo false
    exit 0
  fi
done

# if each letter is found, the answer is true
echo true
