#!/usr/bin/env bash

fail() {
  echo "Invalid number.  [1]NXX-NXX-XXXX N=2-9, X=0-9"
  exit 1
}

# strip non-digits
input=${1//[^0-9]/}

# for 11 digit numbers - first digit must be 1
if [[ ${#input} == 11 ]]
then
 [[ ${input:0:1} == 1 ]] || fail
 input=${input:1:10} # drop first digit
fi

# print number if all conditions true; else fail
[[ ${#input} == 10 ]] && \
[[ ${input:0:1} =~ [2-9] ]] && \
[[ ${input:3:1} =~ [2-9] ]] && \
echo $input || fail
