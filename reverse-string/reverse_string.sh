#!/usr/bin/env bash

# The if-else is a workaround for otherwise omitted spaces.

while read -n1 character; do
  if (( ${#character} == 0 )); then
    reverse="${character} ${reverse}"
  else
    reverse="${character}${reverse}"
  fi
done < <(echo -n "$1")

echo "$reverse"
