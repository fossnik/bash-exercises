#!/usr/bin/env bash

# transcribe rna
for (( i = 0; i < ${#1}; i++ )); do
  case ${1:$i:1} in
  'G') output="$output"'C';;
  'C') output="$output"'G';;
  'T') output="$output"'A';;
  'A') output="$output"'U';;
  *) echo "Invalid nucleotide detected."
     exit 1;;
  esac
done

echo $output
