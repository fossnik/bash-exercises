#!/usr/bin/env bash

decimals=(1000 900 500 400 100 90 50 40 10 9 5 4 1)
numerals=("M" "CM" "D" "CD" "C" "XC" "L" "XL" "X" "IX" "V" "IV" "I")

input=$1

# keep subtracting amounts from largest to smallest
for i in ${!decimals[@]}; do
  while (( $input >= ${decimals[i]} )); do
    output=$output${numerals[$i]}
    ((input-=${decimals[$i]}))
  done
done

echo $output
