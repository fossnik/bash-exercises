#!/usr/bin/env bash

# convert input to all UPPER
input="$(echo $1 | awk '{print toupper($0)}')"
pointSum=0

# scrabble point values
declare -a values='([1]="AEIOULNRST" [2]="DG" [3]="BCMP" [4]="FHVWY" [5]="K" [8]="JX" [10]="QZ")'

# loop through each character in input
for (( i=0; i<${#input}; i++ )); do

    # get point value of the letter
    for p in 1 2 3 4 5 8 10; do
      if [[ ${values[p]} == *"${input:$i:1}"* ]]; then
        pointSum="$((pointSum + p))"
        break
      fi
    done

done

echo ${pointSum}
