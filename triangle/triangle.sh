#!/usr/bin/env bash

# bash doesn't grok floats - but 'bc' does

# triangle inequality violation - zero-length side
if (( `bc <<< "$2 <= 0 || $3 <= 0 || $4 <= 0"` ))
  then echo false; exit
fi

# triangle inequality - one side as large or larger than sum of others
if (( `bc <<< "$2 + $3 <= $4 || $3 + $4 <= $2 || $2 + $4 <= $3"` ))
  then echo false; exit
fi

case $1 in
  "equilateral")
    (( `bc <<< "$2 == $3 && $3 == $4"` )) && \
    echo "true" || \
    echo "false"
    ;;
  "isosceles")
    (( `bc <<< "$2 == $3 || $2 == $4 || $3 == $4"` )) && \
    echo "true" || \
    echo "false"
    ;;
  "scalene")
    (( `bc <<< "$2 != $3 && $2 != $4 && $3 != $4"` )) && \
    echo "true" || \
    echo "false"
    ;;
  *) exit 1;;
esac
