#!/usr/bin/env bash

echo "One for $([[ -n $1 ]] && printf $1 || printf you), one for me."
