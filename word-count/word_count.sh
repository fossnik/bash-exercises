#!/usr/bin/env bash

# use associative array
declare -A tally

input=${1//[^a-Z ]/} # strip undesired characters

# tally up words
for word in ${input,,} # lowercase
do ((tally[$word]++))
done

for i in "${!tally[@]}"
do echo $i: ${tally[$i]}
done
